import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="as_seg",
    version="0.1.4tismir",
    author="Marmoret Axel",
    author_email="axel.marmoret@imt-atlantique.fr",
    description="Package for the segmentation of autosimilarity matrices. This version is related to the publication in the TISMIR journal.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.imt-atlantique.fr/a23marmo/autosimilarity_segmentation/-/tree/TISMIR",
    packages=setuptools.find_packages(),
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Multimedia :: Sound/Audio :: Analysis",
        "Programming Language :: Python :: 3.8"
    ],
    license='BSD',
    install_requires=[
        'librosa == 0.8.1',
        'madmom == 0.16.1',
        'matplotlib >= 1.5',
        'mir_eval',
        'mirdata == 0.3.3',
        'numpy == 1.22.4',
        'pandas',
        'scikit-learn == 1.0.1',
        'scipy == 1.5.4',
        'tensorly == 0.5.1'
    ]
)
