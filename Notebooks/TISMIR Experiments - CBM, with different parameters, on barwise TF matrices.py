#!/usr/bin/env python
# coding: utf-8

# # Experiments related to the CBM algorithm on Barwise TF matrices.
# This notebook allow to reproduce the experiments for the CBM, applied to Barwise TF matrices. The CBM is based on self-similarity matrices, which are precomputed and stored in the data/data_persisted/\<dataset\>/self_similarity_matrices folder.
# 
# You should be able to run this file without additional data, but you may need to update the path to the folder parent of data (we assume that the code is run without modifications, hence that the current directory is the Notebooks one).

# In[1]:


# Traditional imports
import math
import mirdata # For handling annotations of SALAMI
import numpy as np

# Module containing the CBM algorithm
import as_seg.CBM_algorithm as CBM

# Module for manipulating data, 
# in particular pre- and post-processing segments and computing segmentation scores
import as_seg.data_manipulation as dm

# Module for displaying results
import as_seg.model.display_results as display_results

# Module for errors wich could be raised
import as_seg.model.errors as err

# Config files for importants paths, notably where are stored self-similaity matrices and beats/bars estimations.
import as_seg.scripts.default_path as paths
# We suppose that we are in the Notebooks folder, hence data is in the parent folder. If you want to change the path, uncomment the following line and change it accordingly (it should be the parent of the data folder).
# paths.path_parent_of_data = ## TODO: change this line if you are not in the Notebooks folder.

# Scripts for loading stored data.
import as_seg.scripts.overall_scripts as scr


# In[2]:


# Data preprocessing parameters
feature = "log_mel_grill" # Actually the only one with stored self-similarity matrices.
subdivision = 96 # Number of frames per bar

# Parameters for the CBM algorithm
self_similarity_types = ["cosine", "autocorrelation", "rbf"] # Type of self-similarity matrix 
# NB: self-similarity is sometimes refered to as autosimilarity in the code. It should not be the case anymore but some might remain.
bands_numbers = [None,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16] # Note: None represents the case where the self-similarity matrix is not reduced in bands, i.e. the full kernel is used.
penalty_functions = ["target_deviation_8_alpha_half","target_deviation_8_alpha_one","target_deviation_8_alpha_two", "modulo8"]


# In[3]:


# Initialization of the SALAMI dataset
salami = mirdata.initialize('salami', data_home = paths.path_entire_salami)
len_salami = len(salami.track_ids)
salami_test_dataset = scr.get_salami_test_indexes()


# In[4]:


# Parameters for metrics and display of results.
tolerance = "b" # Tolerance for the comutation of segmentation scores.
metrics = ['P0b', 'R0b', 'F0b','P1b', 'R1b', 'F1b']
emphasis_metrics = ['F0b','F1b']


# In[5]:


def train_diff_ssm_kernels_salami(bands_numbers, self_similarity_types):
    """
    Computing scores for the different self-similarity matrices, with different kernels (number of bands), on the train dataset of SALAMI.
    """
    # Array storing results
    results_diff_ssm = math.inf * np.ones((len_salami, len(self_similarity_types), len(bands_numbers), 2, 3)) # Songs, self-similarity types, bands, tol, metrics
    
    # Init of SALAMI
    all_tracks = salami.load_tracks()
    song_idx = 0
            
    for key, track in all_tracks.items(): # Parsing all files in SALAMI
        if scr.is_in_salami_train(int(key), salami_test_dataset): # Train dataset
            try:               
                bars = scr.load_bars("salami", key) # Loading bars, which were precomputed and stored.
  
                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                    
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                # Compute CBM for each self-similarity matrix, with different kernels.
                try:
                    for idx_as, self_similarity_type in enumerate(self_similarity_types):
                        # Load the self-similarity matrix, precomputed and stored.
                        self_similarity_barTF = scr.load_barwise_tf_ssm("salami", key, feature, subdivision, similarity_type = self_similarity_type, train = True)
                        for idx_bn, bands_number in enumerate(bands_numbers): # Compute CBM for each kernel
                            segments = CBM.compute_cbm(self_similarity_barTF, penalty_weight = 0, penalty_func = "modulo8", bands_number = bands_number)[0]                
                            results_diff_ssm[song_idx, idx_as, idx_bn] = dm.get_scores_switch_time_alignment(tolerance, segments, bars, ref_tab) # Compute scores
                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")

    results_diff_ssm = results_diff_ssm[:song_idx] # Keep only the songs which were correctly processed.
    np_avg_diff_as_kernels = np.mean(results_diff_ssm, axis = 0).reshape((len(self_similarity_types), len(bands_numbers), 2, 3)) # Average over songs

    # Display results
    display_results.display_experimental_results(data = np_avg_diff_as_kernels.reshape((len(self_similarity_types)* len(bands_numbers), 6)), 
                                                 conditions = [self_similarity_types, bands_numbers],
                                                 metrics = metrics, emphasis=emphasis_metrics)
    
    # Compute the F-measure averaged over both tolerances, for each self-similarity matrix and each kernel.
    avg_fmes_for_all_params = np.add(np_avg_diff_as_kernels[:,:,0,2], np_avg_diff_as_kernels[:,:,1,2])

    best_self_similarity_full_kernel = display_results.find_best_condition(avg_fmes_for_all_params[:,0], self_similarity_types) # Find the best self-similarity matrix with the full kernel
    best_self_similarity_global, best_bands_number = display_results.find_best_condition(avg_fmes_for_all_params, [self_similarity_types, bands_numbers]) # Find the best self-similarity matrix and kernel, optimized together.
    if best_bands_number is not None: # Cast into int if it is not None (i.e. the full kernel)
        best_bands_number = int(best_bands_number)
    return best_self_similarity_full_kernel, best_bands_number, best_self_similarity_global # return optimal parameters


# In[6]:


print("Training on SALAMI dataset")
best_self_similarity_full_kernel, best_bands_number, best_self_similarity_global = train_diff_ssm_kernels_salami(self_similarity_types=self_similarity_types,bands_numbers = bands_numbers)
print("-------------------------------------------------------------------")
print(f"Best self-similarity when using the full kernel: {best_self_similarity_full_kernel}")
print("-------------------------------------------------------------------")
print(f"Best parameters for all band kernels:\nKernel optimal number of bands: {best_bands_number}, best self-similarity: {best_self_similarity_global}")


# In[7]:


def test_best_ssm_kernel_salami(bands_number, self_similarity_type):
    """
    Computing scores for the previously find best self-similarity matrix, with the best kernel, on the test dataset of SALAMI.
    """
    # Array storing results
    results_diff_as = math.inf * np.ones((len_salami, 2, 3)) # Songs, tol, metrics
    
    # Init of SALAMI
    all_tracks = salami.load_tracks()
    song_idx = 0
        
    for key, track in all_tracks.items(): # Parsing all files in SALAMI
        if scr.is_in_salami_test(int(key), salami_test_dataset): # Test dataset
            try:               
                bars = scr.load_bars("salami", key) # Loading bars, which were precomputed and stored.
  
                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                try:
                    self_similarity_barTF = scr.load_barwise_tf_ssm("salami", key, feature, subdivision, similarity_type = self_similarity_type, train = False) # Load the self-similarity matrix, precomputed and stored.
                    segments = CBM.compute_cbm(self_similarity_barTF, penalty_weight = 0, penalty_func = "modulo8", bands_number = bands_number)[0] # Compute CBM
                    results_diff_as[song_idx] = dm.get_scores_switch_time_alignment(tolerance, segments, bars, ref_tab) # Compute scores
                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")

    results_diff_as = results_diff_as[:song_idx] # Keep only the songs which were correctly processed.
    np_all_avg_res = np.mean(results_diff_as, axis = 0) # Average over songs

    # Display results
    display_results.display_experimental_results(data = np_all_avg_res.reshape((1, 6)), 
                                                 conditions = ["SALAMI dataset"],
                                                 metrics = metrics, emphasis=emphasis_metrics)
    
    return np_all_avg_res # return scores

def test_best_ssm_kernel_rwcpop(bands_number, self_similarity_type):
    """
    Computing scores for the previously find best self-similarity matrix, with the best kernel, on the RWC Pop dataset.
    """
    songs_range = range(1,101) # All songs in the dataset

    # Array storing results
    results_diff_as = math.inf * np.ones((len(songs_range), 2, 3)) # Songs, tol, metrics
        
    for song_idx, song_name in enumerate(songs_range): # Parsing all files in RWC Pop
        bars, references_segments = scr.load_bar_annot_song_RWC(song_name) # Loading bars and annotations of sections
  
        self_similarity_barTF = scr.load_barwise_tf_ssm("rwcpop", song_name, feature, subdivision, similarity_type = self_similarity_type) # Load the self-similarity matrix, precomputed and stored.
        segments = CBM.compute_cbm(self_similarity_barTF, penalty_weight = 0, penalty_func = "modulo8", bands_number = bands_number)[0] # Compute CBM
        results_diff_as[song_idx] = dm.get_scores_switch_time_alignment(tolerance, segments, bars, [references_segments]) # Compute scores

    np_all_avg_res = np.mean(results_diff_as, axis = 0) # Average over songs
    
    # Display results
    display_results.display_experimental_results(data = np_all_avg_res.reshape((1, 6)), 
                                                 conditions = ["RWC Pop dataset"],
                                                 metrics = metrics, emphasis=emphasis_metrics)

    return np_all_avg_res # return scores


# In[8]:


# Scores of the best self-similarity with the full kernel
print("-------------------------------------------------------------------")
print(f"Test scores for the best self-similarity ({best_self_similarity_full_kernel}) with the full kernel")
scores_salami_fk = test_best_ssm_kernel_salami(bands_number = None, self_similarity_type = best_self_similarity_full_kernel)
scores_rwcpop_fk = test_best_ssm_kernel_rwcpop(bands_number = None, self_similarity_type = best_self_similarity_full_kernel)

# Best band kernel
print("-------------------------------------------------------------------")
print(f"Test scores for the best self-similarity ({best_self_similarity_global})  and the optimal kernel ({best_bands_number}-band) on the train dataset")
scores_salami = test_best_ssm_kernel_salami(bands_number = best_bands_number, self_similarity_type = best_self_similarity_global)
scores_rwcpop = test_best_ssm_kernel_rwcpop(bands_number = best_bands_number, self_similarity_type = best_self_similarity_global)


# In[10]:


def train_penalty_function_salami(penalty_functions, bands_number, self_similarity_type):
    """
    Computing scores for the different penalty functions, on the train dataset of SALAMI, with the previously found best self-similarity and kernel.
    """
    penalty_weight_range = np.arange(0.01, 0.2, 0.01) # Range of penalty weights to test

    # Array storing results
    results_diff_pen = math.inf * np.ones((len_salami, len(penalty_functions), len(penalty_weight_range), 2, 3)) # Songs, penalty functions, weight range, tol, metrics
    
    # Init of SALAMI
    all_tracks = salami.load_tracks()
    song_idx = 0
            
    for key, track in all_tracks.items(): # Parsing all files in SALAMI
        if scr.is_in_salami_train(int(key), salami_test_dataset): # Train dataset
            try:               
                bars = scr.load_bars("salami", key) # Loading bars, which were precomputed and stored.

                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                try:
                    # Load the self-similarity matrix, precomputed and stored.
                    self_similarity_barTF = scr.load_barwise_tf_ssm("salami", key, feature, subdivision, similarity_type = self_similarity_type, train = True)

                    for idx_pen, penalty_func in enumerate(penalty_functions): # Parse all penalty functions
                        for idx_weight, weight in enumerate(penalty_weight_range): # Parse all penalty weights
                            segments = CBM.compute_cbm(self_similarity_barTF, penalty_weight = weight, penalty_func = penalty_func, bands_number = bands_number)[0] # Compute CBM          
                            results_diff_pen[song_idx, idx_pen, idx_weight] = dm.get_scores_switch_time_alignment(tolerance, segments, bars, ref_tab) # Compute scores
                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")

    results_diff_pen = results_diff_pen[:song_idx] # Keep only the songs which were correctly processed.
    np_all_avg_res = np.mean(results_diff_pen, axis = 0) # Average over songs

    # Display results
    display_results.display_experimental_results(data = np_all_avg_res.reshape((len(penalty_functions)*len(penalty_weight_range), 6)), 
                                                 conditions = [penalty_functions, penalty_weight_range],
                                                 metrics = metrics, emphasis=emphasis_metrics)
    
    avg_fmes_for_all_params = np.add(np_all_avg_res[:,:,0,2], np_all_avg_res[:,:,1,2]) # Compute the F-measure averaged over both tolerances, for each penalty function and each penalty weight.
    
    # Find the best penalty function and penalty weight, optimized together.
    best_penalty_function, best_weight = display_results.find_best_condition(avg_fmes_for_all_params, [penalty_functions, penalty_weight_range])

    best_weight = np.float64(best_weight) # Cast to float64, for the rest of the code.
    return best_penalty_function, best_weight # return optimal parameters


# In[11]:


print("Training on SALAMI dataset")
best_penalty_function, best_weight = train_penalty_function_salami(penalty_functions, best_bands_number, best_self_similarity_global)
print("-------------------------------------------------------------------")
print(f"Best penalty function: {best_penalty_function}, with weight: {best_weight}")


# In[12]:


def test_best_penalty_function_salami(self_similarity_type, bands_number, penalty_function, penalty_weight):
    """
    Computing scores for the previously found best penalty function, on the test dataset of SALAMI.
    """
    # Array storing results
    results_best_pen = math.inf * np.ones((len_salami, 2, 3)) # Songs, tol, metrics
    
    # Init of SALAMI
    all_tracks = salami.load_tracks()
    song_idx = 0
        
    for key, track in all_tracks.items(): # Parsing all files in SALAMI
        if scr.is_in_salami_test(int(key), salami_test_dataset): # Test dataset
            try:               
                bars = scr.load_bars("salami", key) # Loading bars, which were precomputed and stored.
  
                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                try:
                    self_similarity_barTF = scr.load_barwise_tf_ssm("salami", key, feature, subdivision, similarity_type = self_similarity_type, train = False) # Load the self-similarity matrix, precomputed and stored.
                    segments = CBM.compute_cbm(self_similarity_barTF, penalty_weight = penalty_weight, penalty_func = penalty_function, bands_number = bands_number)[0] # Compute CBM
                    results_best_pen[song_idx] = dm.get_scores_switch_time_alignment(tolerance, segments, bars, ref_tab) # Compute scores
                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")

    results_best_pen = results_best_pen[:song_idx] # Keep only the songs which were correctly processed.
    np_all_avg_res = np.mean(results_best_pen, axis = 0) # Average over songs
    
    # Display results
    display_results.display_experimental_results(data = np_all_avg_res.reshape((1, 6)), 
                                                 conditions = ["SALAMI dataset"],
                                                 metrics = metrics, emphasis=emphasis_metrics)

    return np_all_avg_res # return scores

def test_best_penalty_function_rwcpop(bands_number, self_similarity_type, penalty_function, penalty_weight):
    """
    Computing scores for the previously found best penalty function, on the RWC Pop dataset.
    """
    songs_range = range(1,101) # All songs in the dataset
    
    # Array storing results
    results_best_pen = math.inf * np.ones((len(songs_range), 2, 3)) # Songs, tol, metrics
        
    for song_idx, song_name in enumerate(songs_range): # Parsing all files in RWC Pop
        bars, references_segments = scr.load_bar_annot_song_RWC(song_name) # Loading bars and annotations of sections
        self_similarity_barTF = scr.load_barwise_tf_ssm("rwcpop", song_name, feature, subdivision, similarity_type = self_similarity_type) # Load the self-similarity matrix, precomputed and stored.
        segments = CBM.compute_cbm(self_similarity_barTF, penalty_weight = penalty_weight, penalty_func = penalty_function, bands_number = bands_number)[0] # Compute CBM
        results_best_pen[song_idx] = dm.get_scores_switch_time_alignment(tolerance, segments, bars, [references_segments]) # Compute scores

    np_all_avg_res = np.mean(results_best_pen, axis = 0) # Average over songs

    # Display results
    display_results.display_experimental_results(data = np_all_avg_res.reshape((1, 6)), 
                                                 conditions = ["RWC Pop dataset"],
                                                 metrics = metrics, emphasis=emphasis_metrics)

    return np_all_avg_res # return scores


# In[13]:


# Best penalty function
print("-------------------------------------------------------------------")
print(f"Test scores for the best penalty function ({best_penalty_function}, with weight {best_weight}) with the best self-similarity ({best_self_similarity_global}) and the optimal kernel ({best_bands_number}-band) on the train dataset")
scores_salami = test_best_penalty_function_salami(bands_number = best_bands_number, self_similarity_type = best_self_similarity_global, penalty_function=best_penalty_function, penalty_weight=best_weight)
scores_rwcpop = test_best_penalty_function_rwcpop(bands_number = best_bands_number, self_similarity_type = best_self_similarity_global, penalty_function=best_penalty_function, penalty_weight=best_weight)

