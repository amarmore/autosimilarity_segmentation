#!/usr/bin/env python
# coding: utf-8

# # Experiments related to the CBM algorithm on Beatwise TF matrices.
# This notebook allow to reproduce the experiments for the CBM, applied to Beatwise TF matrices. The CBM is based on self-similarity matrices, which are precomputed and stored in the data/data_persisted/\<dataset\>/self_similarity_matrices folder.
# 
# ## Due to the size of the SSM for Beatwise TF matrices, they are not included in the git folder.
# They can be found in the Zenodo upload: 10.5281/zenodo.10168387. We apologize for the incovenience.
# 
# Still, with the Zenodo folder, you should be able to run this file without additional data, but you may need to update the path to the folder parent of data (we assume that the code is run without modifications, hence that the current directory is the Notebooks one). Data should be set at the same hierarchy than Notebooks.

# In[1]:


# Traditional imports
import math
import matplotlib.pyplot as plt
import mirdata # For handling annotations of SALAMI
import numpy as np

# Module containing the CBM algorithm
import as_seg.CBM_algorithm as CBM

# Module for manipulating data, 
# in particular pre- and post-processing segments and computing segmentation scores
import as_seg.data_manipulation as dm

# Module for displaying results
import as_seg.model.display_results as display_results

# Module for errors wich could be raised
import as_seg.model.errors as err

# Config files for importants paths, notably where are stored self-similaity matrices and beats/bars estimations.
import as_seg.scripts.default_path as paths
# We suppose that we are in the Notebooks folder, hence data is in the parent folder. If you want to change the path, uncomment the following line and change it accordingly (it should be the parent of the data folder).
# paths.path_parent_of_data = ## TODO: change this line if you are not in the Notebooks folder.

# Scripts for loading stored data.
import as_seg.scripts.overall_scripts as scr


# In[2]:


# Data preprocessing parameters
feature = "log_mel_grill" # Actually the only one with stored self-similarity matrices.
subdivision_beat = 24 # Number of frames per beat

# Parameters for the CBM algorithm
self_similarity_types = ["cosine", "autocorrelation", "rbf"]
beatwise_band_numbers = np.concatenate((np.arange(1, 19, 2), np.arange(19, 70, 4), [32, 64]))


# In[ ]:


# Initialization of the SALAMI dataset
salami = mirdata.initialize('salami', data_home = paths.path_entire_salami)
len_salami = len(salami.track_ids)
salami_test_dataset = scr.get_salami_test_indexes()


# In[ ]:


# Parameters for metrics and display of results.
metrics = ['P0.5', 'R0.5', 'F0.5','P3', 'R3', 'F3']
emphasis_metrics = ['F0.5', 'F3']


# In[3]:


def train_diff_ssm_salami(self_similarity_types):
    """
    Computes the CBM algorithm on the different beatwise self-similarity matrices of the SALAMI dataset, with the full kernel.
    """
    # Initialization of the results table
    results_diff_ssm = math.inf * np.ones((len_salami, len(self_similarity_types), 2, 3)) # Songs, self-similarity types, tol, metrics
    
    # Initialization of the SALAMI dataset
    all_tracks = salami.load_tracks()
    song_idx = 0

    for key, track in all_tracks.items(): # For each song in the SALAMI dataset
        if scr.is_in_salami_train(int(key), salami_test_dataset): # Train dataset
            try:
                beats = scr.load_beats('salami', key) # Load the beats estimations, precomputed and stored.

                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                try:
                    for idx_as, self_similarity_type in enumerate(self_similarity_types): # For each self-similarity
                        self_similarity_beatTF = scr.load_beatwise_tf_ssm("salami", key, feature, subdivision_beat, similarity_type = self_similarity_type, train = True) # Load the self-similarity matrix, precomputed and stored.
                        segments = CBM.compute_cbm(self_similarity_beatTF, max_size = 128, penalty_weight = 0, penalty_func = "modulo8", bands_number = None)[0] # Compute the CBM algorithm on the self-similarity matrix
                        results_diff_ssm[song_idx, idx_as] = dm.get_scores_in_time_from_barwise_segments(segments, beats, ref_tab) # Compute the scores of the segmentation

                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")            
                
            except MemoryError:
                print(f"{key} too large")
            
            except err.ToDebugException:
                print(f"{key}: duplicate samples when computing the beatwise TF matrix")

    results_diff_ssm = results_diff_ssm[:song_idx] # Keep only the songs which were correctly processed.
    np_avg_diff_as = np.mean(results_diff_ssm, axis = 0).reshape((len(self_similarity_types), 2, 3)) # Compute the average scores of the segmentation for each self-similarity matrix.
    
    # Display the results
    display_results.display_experimental_results(data = np_avg_diff_as.reshape((len(self_similarity_types), 6)), conditions = np.array([f"Self-similarity: {current_as}" for current_as in self_similarity_types]),metrics = metrics, emphasis=emphasis_metrics)
    
    avg_fmes_for_all_params = np.add(np_avg_diff_as[:,0,2], np_avg_diff_as[:,1,2]) # Compute the F-measure (averaged between both tolerances) for each self-similarity matrix.
    best_self_similarity_full_kernel = display_results.find_best_condition(avg_fmes_for_all_params, self_similarity_types) # Find the best self-similarity matrix.
    return best_self_similarity_full_kernel

def train_diff_bands_kernels_salami(bands_numbers, self_similarity_type):
    """
    Computes the CBM algorithm with different kernels (different band numbers) of the SALAMI dataset, with the previosuly found best self-similarity matrix.
    """
    # Initialization of the results table
    results_diff_kernels = math.inf * np.ones((len_salami, len(bands_numbers), 2, 3)) # Songs, bands, tol, metrics
    
    # Initialization of the SALAMI dataset
    all_tracks = salami.load_tracks()
    song_idx = 0
           
    for key, track in all_tracks.items(): # For each song in the SALAMI dataset
        if scr.is_in_salami_train(int(key), salami_test_dataset): # Train dataset
            try:
                beats = scr.load_beats('salami', key) # Load the beats estimations, precomputed and stored.

                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                try:
                    self_similarity_beatTF = scr.load_beatwise_tf_ssm("salami", key, feature, subdivision_beat, similarity_type = self_similarity_type, train = True) # Load the self-similarity matrix, precomputed and stored.
                    for idx_bn, bands_number in enumerate(bands_numbers): # For each kernel
                        segments = CBM.compute_cbm(self_similarity_beatTF, penalty_weight = 0, max_size = 128, penalty_func = "modulo8", bands_number = bands_number)[0] # Compute the CBM algorithm on the self-similarity matrix
                        results_diff_kernels[song_idx, idx_bn] = dm.get_scores_in_time_from_barwise_segments(segments, beats, ref_tab) # Compute the scores of the segmentation

                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")            
                
            except MemoryError:
                print(f"{key} too large")
            
            except err.ToDebugException:
                print(f"{key}: duplicate samples when computing the beatwise TF matrix")

    results_diff_kernels = results_diff_kernels[:song_idx] # Keep only the songs which were correctly processed.
    np_avg_diff_kernel = np.mean(results_diff_kernels, axis = 0).reshape((len(bands_numbers), 2, 3)) # Compute the average scores of the segmentation for each kernel.

    # Display the results
    display_results.display_experimental_results(data = np_avg_diff_kernel.reshape((len(bands_numbers), 6)), 
                                                 conditions = np.array([f"Kernel: {current_kernel}-band" for current_kernel in bands_numbers]),
                                                 metrics = metrics, emphasis=emphasis_metrics)
    
    avg_fmes_for_all_params = np.add(np_avg_diff_kernel[:,0,2], np_avg_diff_kernel[:,1,2]) # Compute the F-measure (averaged between both tolerances) for each kernel.
    best_kernel_this_self_similarity = display_results.find_best_condition(avg_fmes_for_all_params, bands_numbers) # Find the best kernel.
    
    if best_kernel_this_self_similarity is not None: # Cast into int if it is not None (i.e. the full kernel)
        best_kernel_this_self_similarity = int(best_kernel_this_self_similarity)
    return best_kernel_this_self_similarity # Return the best kernel.


# In[4]:


# Train on the salami train to find the best self-similarity matrix, with the full kernel.
# This training take approximately 1 hour.
print("----------------------------------------")
print("Training on SALAMI train to find the best self-similarity matrix, with the full kernel")
best_self_similarity_full_kernel = train_diff_ssm_salami(self_similarity_types)
print(f"Best self-similarity matrix: {best_self_similarity_full_kernel}")


# In[5]:


# Train on the salami train to find the best kernel (number of bands).
# This one is reeeeaally long, it took almost 10 hours to compute on my computer.
print("----------------------------------------")
print(f"Training on SALAMI train to find the best kernel, with the {best_self_similarity_full_kernel} self-similarity matrix")
best_kernel = train_diff_bands_kernels_salami(beatwise_band_numbers, best_self_similarity_full_kernel) #range(2,65)
print(f"Best kernel: {best_kernel}-band")


# In[6]:


def test_best_ssm_kernel_salami(bands_number, self_similarity_type):
    """
    Testing the best self-similarity matrix and kernel on the SALAMI test dataset.
    """
    # Initialization of the results table
    results_diff_ssm = math.inf * np.ones((len_salami, 2, 3)) # Songs, tol, metrics
    
    # Initialization of the SALAMI dataset
    all_tracks = salami.load_tracks()
    song_idx = 0
        
    for key, track in all_tracks.items(): # For each song in the SALAMI dataset
        if scr.is_in_salami_test(int(key), salami_test_dataset): # Test dataset
            try:               
                beats = scr.load_beats('salami', key) # Load the beats estimations, precomputed and stored.

                # Loading annotations of sections, for both annotators if both have annotated.
                ref_tab = []
                try:
                    references_segments = salami.load_sections(track.sections_annotator1_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass
                try:
                    references_segments = salami.load_sections(track.sections_annotator2_uppercase_path).intervals
                    ref_tab.append(references_segments)
                except (TypeError, AttributeError):
                    pass

                try:
                    self_similarity_beatTF = scr.load_beatwise_tf_ssm("salami", key, feature, subdivision_beat, similarity_type = self_similarity_type, train = False) # Load the self-similarity matrix, precomputed and stored.
                    segments = CBM.compute_cbm(self_similarity_beatTF, max_size = 128, penalty_weight = 0, penalty_func = "modulo8", bands_number = bands_number)[0] # Compute the CBM algorithm on the self-similarity matrix
                    results_diff_ssm[song_idx] = dm.get_scores_in_time_from_barwise_segments(segments, beats, ref_tab) # Compute the scores of the segmentation
                    song_idx += 1
                except TypeError:
                    print(f"Error in test at song {key}, {track}")
    
            except FileNotFoundError:
                print(f"{key} not found, normal ?")
        
            except MemoryError:
                print(f"{key} too large")

            except err.ToDebugException:
                print(f"{key}: duplicate samples when computing the beatwise TF matrix")

    results_diff_ssm = results_diff_ssm[:song_idx] # Keep only the songs which were correctly processed.
    np_all_avg_res = np.mean(results_diff_ssm, axis = 0) # Compute the average scores of the segmentation for each kernel.
    
    # Display the results
    display_results.display_experimental_results(data = np_all_avg_res.reshape(1, 6), conditions = ["Results on SALAMI"], metrics = metrics)

    return np_all_avg_res # Return the average scores of the segmentation for each kernel.

def test_best_ssm_kernel_rwcpop(bands_number, self_similarity_type):
    """
    Testing the best self-similarity matrix and kernel on the RWC Pop dataset.
    """
    songs_range = range(1,101) # All the songs in the dataset

    # Initialization of the results table
    results_diff_ssm = math.inf * np.ones((len(songs_range), 2, 3)) # Songs, tol, metrics
        
    for song_idx, song_name in enumerate(songs_range): # For each song in the RWC Pop dataset
        beats, references_segments = scr.load_beat_annot_song_RWC(song_name) # Load the beats estimations and the annotations of sections, precomputed and stored.
        self_similarity_beatTF = scr.load_beatwise_tf_ssm("rwcpop", song_name, feature, subdivision_beat, similarity_type = self_similarity_type) # Load the self-similarity matrix, precomputed and stored.
        segments = CBM.compute_cbm(self_similarity_beatTF, max_size = 128, penalty_weight = 0, penalty_func = "modulo8", bands_number = bands_number)[0] # Compute the CBM algorithm on the self-similarity matrix
        results_diff_ssm[song_idx] = dm.get_scores_in_time_from_barwise_segments(segments, beats, [references_segments]) # Compute the scores of the segmentation

    np_all_avg_res = np.mean(results_diff_ssm, axis = 0) # Compute the average scores of the segmentation.
    display_results.display_experimental_results(data = np_all_avg_res.reshape(1, 6), conditions = ["Results on RWC Pop"], metrics = metrics) # Display the results

    return np_all_avg_res # Return the average scores of the segmentation.


# In[7]:


# Best band kernel
scores_test_salami = test_best_ssm_kernel_salami(bands_number = best_kernel, self_similarity_type = best_self_similarity_full_kernel)
scores_test_rwcpop = test_best_ssm_kernel_rwcpop(bands_number = best_kernel, self_similarity_type = best_self_similarity_full_kernel)

