#!/usr/bin/env python
# coding: utf-8

# # Experiments related to Foote's algorithm with different discretizations.
# This notebook allow to reproduce the experiments for Foote's algorithm, comparing different discretizations. Foote's algorithm is based on self-similarity matrices, which are precomputed and stored in the data/data_persisted/\<dataset\>/foote_experiments folder.
# 
# You should be able to run this file without additional data, but you may need to update the path to the folder parent of data (we assume that the code is run without modifications, hence that the current directory is the Notebooks one).

# In[1]:


# Traditional imports
import math
import mirdata # For handling annotations of SALAMI
import numpy as np

# Module for manipulating data, 
# in particular pre- and post-processing segments and computing segmentation scores
import as_seg.data_manipulation as dm

# Module containing our encapsulation of the code of Foote, based on MSAF implementation.
import as_seg.foote_novelty as foote

# Module for displaying results
import as_seg.model.display_results as display_results

# Module for errors wich could be raised
import as_seg.model.errors as err

# Config files for importants paths, notably where are stored self-similaity matrices and beats/bars estimations.
import as_seg.scripts.default_path as paths
# We suppose that we are in the Notebooks folder, hence data is in the parent folder. If you want to change the path, uncomment the following line and change it accordingly (it should be the parent of the data folder).
# paths.path_parent_of_data = ## TODO: change this line if you are not in the Notebooks folder.

# Scripts for loading stored data.
import as_seg.scripts.overall_scripts as scr


# In[2]:


paths.path_data_persisted


# In[3]:


# Feature scale params
M_gaussian_beat_scale = 66 # Size of Foote's kernel
L_peaks_beat_scale = 64 # Hyperparameter for peaks selection

# Barwise params
subdivision = 96 # Number of frames per bar
M_gaussian_barwise = 16 # Size of Foote's kernel
L_peaks_barwise = 16 # Hyperparameter for peaks selection


# In[4]:


# Parameters for display of results.
indexes_labels_beatwise = ["Beat synchronized (madmom estimates)", "Beat synchronized (madmom estimates), re-aligned on bars"]
indexes_labels_barwise = ["Bar synchronized", "Barwise TF"]
experimental_conditions = np.array(indexes_labels_beatwise + indexes_labels_barwise)

metrics = ['P0.5', 'R0.5', 'F0.5','P3', 'R3', 'F3']
emphasis_metrics = ['F0.5', 'F3']


# In[5]:


def test_salami_diff_discretizations():  
    """
    Testing segmentation scores on the SALAMI dataset
    """    
    # Initialisation of the SALAMI dataset and annotations
    salami = mirdata.initialize('salami', data_home = paths.path_entire_salami)
    len_salami = len(salami.track_ids)
    all_tracks = salami.load_tracks()
    salami_test_dataset = scr.get_salami_test_indexes()

    # Init of tables storing segmentation scores
    results_beatwise = -math.inf * np.ones((len_salami, 2, 2, 3))
    results_barwise = -math.inf * np.ones((len_salami, 2, 2, 3))

    idx_song = 0

    for key, track in all_tracks.items(): # Parsing songs in the dataset
        if scr.is_in_salami_test(int(key), salami_test_dataset): # Testing if the file is in the test dataset
            try:
                bars = scr.load_bars('salami', key) # Load bar estimations
                # Load self-similarity matrices for the beat synchronized matrix
                beat_sync_ssm, beat_sync_times, duration = scr.load_beat_sync_ssm_foote('salami', key)
                # Load self-similarity matrices for the bar synchronized matrix
                bar_sync_ssm, bar_sync_times, duration = scr.load_bar_sync_ssm_foote('salami', key)
                # Load self-similarity matrices for the Barwise TF matrix
                barwiseTF_ssm = scr.load_barwise_tf_ssm_foote('salami', key)

                ref_tab = [] # Loading annotations (may contain two annotations in SALAMI)
                try:
                    ref_tab.append(salami.load_sections(track.sections_annotator1_uppercase_path).intervals)
                except (TypeError, AttributeError):
                    pass
                try:
                    ref_tab.append(salami.load_sections(track.sections_annotator2_uppercase_path).intervals)
                except (TypeError, AttributeError):
                    pass

                try:          
                    # Results at the scale of the beat, madmom estimates of beats (beat-sync features)
                    ## Estimate boundaries
                    beatwise_foote_bndr, labels = foote.process_msaf_own_as(input_ssm = beat_sync_ssm, M_gaussian = M_gaussian_beat_scale, L_peaks = L_peaks_beat_scale)
                    ## Convert boundaries in absolute time
                    beatwise_bndr_foote_in_time, _ = foote.my_process_segmentation_level(beatwise_foote_bndr, labels, beat_sync_ssm.shape[0], beat_sync_times,duration)
                    ## Compute segments from boundaries
                    beatwise_segments_foote_in_time = dm.frontiers_to_segments(list(beatwise_bndr_foote_in_time))
                    ## Compute segmentation scores
                    results_beatwise[idx_song, 0] = dm.get_scores_from_segments_in_time(beatwise_segments_foote_in_time, ref_tab)

                    # beat-level, madmom estimates of beats, realigned on bars
                    segments_beat_scale_aligned_on_bars = dm.align_segments_on_bars(beatwise_segments_foote_in_time, bars)
                    ## Compute segmentation scores
                    results_beatwise[idx_song, 1] = dm.get_scores_from_segments_in_time(segments_beat_scale_aligned_on_bars, ref_tab)

                    # Results at the scale of the bar (bar-sync features)
                    ## Best barwise sync: Barwise sync, Pre filter: 0, Post filter: 2
                    ### These values were obtained in train/test conditions,
                    ### but self-similarity matrices are not stored to avoid large memory consumption.
                    ## Estimate boundaries
                    bar_sync_foote_bndr, labels = foote.process_msaf_own_as(input_ssm = bar_sync_ssm, M_gaussian = M_gaussian_barwise, L_peaks = L_peaks_barwise, pre_filter = 0, post_filter = 2)
                    ## Convert boundaries in absolute time
                    bar_sync_bndr_foote_in_time, _ = foote.my_process_segmentation_level(bar_sync_foote_bndr, labels, bar_sync_ssm.shape[0], bar_sync_times,duration)
                    ## Compute segments from boundaries
                    bar_sync_segments_foote_in_time = dm.frontiers_to_segments(list(bar_sync_bndr_foote_in_time))
                    ## Compute segmentation scores
                    results_barwise[idx_song, 0] = dm.get_scores_from_segments_in_time(bar_sync_segments_foote_in_time, ref_tab)

                    # Results at the scale of the bar (barwise TF matrix)
                    ## Best barwise TF: Barwise sync, Pre filter: 0, Post filter: 1
                    ### These values were obtained in train/test conditions,
                    ### but self-similarity matrices are not stored to avoid large memory consumption.
                    ## Estimate boundaries
                    barwise_foote_bndr = foote.process_msaf_own_as(input_ssm = barwiseTF_ssm, M_gaussian = M_gaussian_barwise, L_peaks = L_peaks_barwise, pre_filter = 0, post_filter = 1)[0]
                    ## Convert boundaries in segments in absolute time
                    barwise_segments_foote_in_time = dm.segments_from_bar_to_time(dm.frontiers_to_segments(list(barwise_foote_bndr)), bars)
                    ## Compute segmentation scores
                    results_barwise[idx_song, 1] = dm.get_scores_from_segments_in_time(barwise_segments_foote_in_time, ref_tab)

                    idx_song += 1

                except TypeError:
                    print(f"Error in test at song {key}, {track}")

            except FileNotFoundError: # Error to handle songs which are not present in the author's machine
                print(f"{key} not found, normal ?")

    print(f"Tested on {idx_song} songs")
    results_beatwise_cropped = results_beatwise[:idx_song] # Keeping only the estimated songs
    results_barwise_cropped = results_barwise[:idx_song] # Keeping only the estimated songs
    
    # Computing the average of all conditions and metrics accross all songs
    np_all_avg_res_line = np.concatenate([np.mean(results_beatwise_cropped, axis = 0).reshape((2,6)), np.mean(results_barwise_cropped, axis = 0).reshape((2,6))], axis = 0)
    # Displaying scores
    display_results.display_experimental_results(data = np_all_avg_res_line, conditions = experimental_conditions, metrics = metrics, emphasis = emphasis_metrics)
    return np_all_avg_res_line
    
def test_rwcpop_diff_discretizations():    
    """
    Testing segmentation scores on the RWC Pop dataset
    Note:  we don't use mirdata because it only contains the AIST annotations, and not the MIREX10 ones.
    """
    # All songs in the RWC Pop dataset
    songs_range = range(1,101)

    # Init of tables storing segmentation scores
    results_beatwise = -math.inf * np.ones((len(songs_range), 2, 2, 3))
    results_barwise = -math.inf * np.ones((len(songs_range), 2, 2, 3))
    
    for idx_song, song_name in enumerate(songs_range): # Parsing songs in the dataset
        # Load bar estimations and annotations
        bars, references_segments = scr.load_bar_annot_song_RWC(song_name)
        
        # Load self-similarity matrices for the beat synchronized matrix
        beat_sync_ssm, beat_sync_times, duration = scr.load_beat_sync_ssm_foote('rwcpop', song_name)
        # Load self-similarity matrices for the bar synchronized matrix
        bar_sync_ssm, bar_sync_times, duration = scr.load_bar_sync_ssm_foote('rwcpop', song_name)
        # Load self-similarity matrices for the Barwise TF matrix
        barwiseTF_ssm = scr.load_barwise_tf_ssm_foote('rwcpop', song_name)

        # Results at the scale of the beat, madmom estimates (beat-sync features)
        ## Estimate boundaries
        beatwise_foote_bndr, labels = foote.process_msaf_own_as(input_ssm = beat_sync_ssm, M_gaussian = M_gaussian_beat_scale, L_peaks = L_peaks_beat_scale)
        ## Convert boundaries in absolute time
        beatwise_bndr_foote_in_time, _ = foote.my_process_segmentation_level(beatwise_foote_bndr, labels, beat_sync_ssm.shape[0], beat_sync_times,duration)
        ## Compute segments from boundaries
        beatwise_segments_foote_in_time = dm.frontiers_to_segments(list(beatwise_bndr_foote_in_time))
        ## Compute segmentation scores
        results_beatwise[idx_song, 0] = dm.get_scores_from_segments_in_time(beatwise_segments_foote_in_time, references_segments)

        # beat-level, madmom estimates, realigned on bars
        ## Align estimates on bars
        segments_beat_scale_aligned_on_bars = dm.align_segments_on_bars(beatwise_segments_foote_in_time, bars)
        ## Compute segmentation scores
        results_beatwise[idx_song, 1] = dm.get_scores_from_segments_in_time(segments_beat_scale_aligned_on_bars, references_segments)

        # Results at the scale of the bar (bar-sync features)
        ## Best barwise sync: Barwise sync, Pre filter: 0, Post filter: 2
        ### These values were obtained in train/test conditions,
        ### but self-similarity matrices are not stored to avoid large memory consumption.
        ## Estimate boundaries
        bar_sync_foote_bndr, labels = foote.process_msaf_own_as(input_ssm = bar_sync_ssm, M_gaussian = M_gaussian_barwise, L_peaks = L_peaks_barwise, pre_filter = 0, post_filter = 2)
        ## Convert boundaries in absolute time
        bar_sync_bndr_foote_in_time, _ = foote.my_process_segmentation_level(bar_sync_foote_bndr, labels, bar_sync_ssm.shape[0], bar_sync_times,duration)
        ## Compute segments from boundaries
        bar_sync_segments_foote_in_time = dm.frontiers_to_segments(list(bar_sync_bndr_foote_in_time))
        ## Compute segmentation scores
        results_barwise[idx_song, 0] = dm.get_scores_from_segments_in_time(bar_sync_segments_foote_in_time, references_segments)

        # Results at the scale of the bar (barwise TF matrix)
        ## Best barwise TF: Barwise sync, Pre filter: 0, Post filter: 1 
        ### These values were obtained in train/test conditions,
        ### but self-similarity matrices are not stored to avoid large memory consumption.
        ## Estimate boundaries
        barwise_foote_bndr = foote.process_msaf_own_as(input_ssm = barwiseTF_ssm, M_gaussian = M_gaussian_barwise, L_peaks = L_peaks_barwise, pre_filter = 0, post_filter = 1)[0]
        ## Convert boundaries in segments in absolute time
        barwise_segments_foote_in_time = dm.segments_from_bar_to_time(dm.frontiers_to_segments(list(barwise_foote_bndr)), bars)
        ## Compute segmentation scores
        results_barwise[idx_song, 1] = dm.get_scores_from_segments_in_time(barwise_segments_foote_in_time, references_segments)
        
    # Computing the average of all conditions and metrics accross all songs
    np_all_avg_res_line = np.concatenate([np.mean(results_beatwise, axis = 0).reshape((2,6)), np.mean(results_barwise, axis = 0).reshape((2,6))], axis = 0)
    # Displaying scores
    display_results.display_experimental_results(data = np_all_avg_res_line, conditions = experimental_conditions, metrics = metrics, emphasis = emphasis_metrics)
    
    return np_all_avg_res_line


# In[6]:


print("SALAMI")
scores_salami = test_salami_diff_discretizations()
print("RWC Pop")
scores_rwcpop = test_rwcpop_diff_discretizations()

