﻿# as_seg #

This project is the archived version of as_seg, because projects are moved to https://gitlab.imt-atlantique.fr/a23marmo/. You can find the folder at the following address: https://gitlab.imt-atlantique.fr/a23marmo/autosimilarity_segmentation
